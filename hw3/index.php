<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <header>
        <img width="200" height="200" src="../LogoMospolytech.jpg" alt="">
        <h1>Equation</h1>
    </header>
    <main> 
        <?php
            $equation = "x + 67 = 129";
            $unknown = "x";
            $res = "129";
            $sum2 = "67";

            $position = stripos($equation, $unknown);

            echo "($unknown) из уравнения ($equation) в позиции ($position)"."<br>";
            
            $mlt = "*";
            $div = "/";
            $sum = "+";
            $mns = "-";
            $possum = (boolean)stripos($equation, $sum);
            $posdiv = (boolean)stripos($equation, $div);
            $posmlt = (boolean)stripos($equation, $mlt);
            $posmns = (boolean)stripos($equation, $mns);

            if ($posmns === true) {
                echo "В уравнении ($equation) находится оператор ($mns)"."<br>";
            } else if ($posdiv === true) {
                echo "В уравнении ($equation) находится оператор ($div)"."<br>";
            } else if ($posmlt === true) {
                echo "В уравнении ($equation) находится оператор ($mlt)"."<br>";
            } else if ($possum === true) {
                echo "В уравнении ($equation) находится оператор ($sum)"."<br>";
                echo "Значение переменной Х: ";
                echo $res - $sum2."<br>";
            } else {
                echo "Оператор не найден.";
            }
            
            //Над решением домашней работы думал совместно с Коршуновым Даниилом, потому возможны сходства.
        ?>

    <img src="block-scheme.jpg" alt="">
    </main>
    <footer>
        <div>
        <p>Написать программу для решения заданного уравнения. Которая будет определять оператор в заданном уравнении и расположение неизвестной переменной.
            Находить значение переменной. Нарисовать блок-схему алгоритма работы программы.</p>
        </div>
    </footer>
</body>
</html>