<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <header>
        <img width="200" height="200" src="../LogoMospolytech.jpg" alt="">
        <h1>Feedback form</h1>
    </header>
    <main>
    <?php
        $id="https://httpbin.org/post";
        $function=get_headers($id);
    ?>
    <textarea name="" id="" cols="60" rows="20">
        <?php
            print_r($function);
        ?>
    </textarea>
    </main>
    <footer>
        <p>1 страница: сверстать форму обратной связи. Отправку формы осуществить на URL: https://httpbin.org/post.
            Добавить кнопку для перехода на 2 страницу. 2 страница: вывести на страницу результат работы функции get_headers.
            Загрузить код в удалённый репозиторий. Залить на хостинг</p>
    </footer>
</body>
</html>